$(document).ready(function(){






























$('.content').fullpage({
  verticalCentered: false,
  controlArrows: false,
  autoScrolling: false,
  fitToSection: false,
  slidesNavigation: true,
  anchors: ['front', 'guide', 'life', 'business', 'relationship', 'yield', 'auto', 'opportunities', 'network', 'contacts'],
  menu: '.menu',
  fixedElements: '.apply',
  afterRender: function () {
      setInterval(function () {
          $.fn.fullpage.moveSlideRight();
      }, 5000);
  },
  afterLoad: function(anchorLink, index) {
    $(this).find('.reveal').removeClass('reveal--visible');
    $(this).find('.fp-slidesNav').removeClass('fp-slidesNav--visible');
    $(this).find('.reveal').addClass('reveal--visible');
    $(this).find('.fp-slidesNav').addClass('fp-slidesNav--visible');
  },
  onLeave: function (index, nextIndex, direction) {
    
    if (index == 1 && direction == 'down') {
      document.getElementById("front__video").pause();
      $('.menu').removeClass('menu--hidden');
    } else if (index == 2 && direction == 'up') {
      document.getElementById("front__video").play();
      $('.menu').addClass('menu--hidden');
    }
    
    if (index == 6 && direction == 'down') {
      $('.menu').addClass('menu--hidden');
      $('.auto__content').fadeOut(3000);
      setTimeout(function(){
        $('.auto__video').fadeIn(1000);
        document.getElementById("auto__video").play();
      }, 2000);
    } else if (index == 7 && direction == 'up') {
      document.getElementById("auto__video").pause();
      $('.menu').removeClass('menu--hidden');
    }
    
    if (index == 7 && direction == 'down') {
      $('.menu').removeClass('menu--hidden');
    } else if (index == 8 && direction == 'up') {
      $('.menu').addClass('menu--hidden');
    }
    
    if (index == 9 && direction == 'down') {
      $('.menu').addClass('menu--hidden');
    } else if (index == 10 && direction == 'up') {
      $('.menu').removeClass('menu--hidden');
    }
  }
});

$('.timeline__content').perfectScrollbar();


window.sr = ScrollReveal()

sr.reveal('.logo', { 
  origin: 'left',
  duration: 2000,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  distance: '100rem',
  mobile: false,
  reset: false,
  opacity: 0,
  viewFactor: .0001,
});

$('.front__play').click(function(e) {
  e.preventDefault();
  $('.front__content').fadeOut(2000);
  $('.front__video').fadeIn(1000);
  document.getElementById("front__video").play();
});

var video = document.getElementsByTagName('video')[0];

video.onended = function(e) {
  $('.front__mouse').css('display', 'flex');
};

$(".fancybox").fancybox({
  afterLoad: function() {
    $('.modal').perfectScrollbar('update')
  },
});

$('.modal').perfectScrollbar();

$('.modal__close').click(function() {
  $.fancybox.close();
});})